package lt.vcs;

import static lt.vcs.VcsUtils.*;


/**
 *
 * @author Ovidas
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {        
        Bankomatas bank = new Bankomatas();
       
        while (bank.getCash() > 0) {
            Person per = new Person(inStr("Iveskite savo varda"));

            String pass = inStr("Iveskite pin koda");
            boolean x = bank.logIn(pass);
            if (x == false) {
                out("neteisingai ivestas pin koodas");
               break;
            }
            out(per.getName() + ", bankomate liko pinigu: " + bank.getCash());
            int pinigai = inInt("Kiek pinigu norite issimti?");
            if (pinigai > bank.getCash()) {
                out("Pinigu bankomate nera");
                break;
            }
            int choise = inInt("Ka norite daryti toliau?: 1-noriu testi, 2-exit");
            if (choise == 2) {
                break;
            }
            
            bank.isimti(pinigai);      
 
            
        }
    }
    
}
