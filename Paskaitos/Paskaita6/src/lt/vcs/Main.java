package lt.vcs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import static lt.vcs.VcsUtils.*;
/**
 *
 * @author Ovidas
 */
public class Main {
    private static final String NL = System.lineSeparator();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        String kelias = inStr("Nurodikite kur yra failas");
        File failas = new File(kelias);                   //kelias  "C:/pvz.txt"
        out("Failas egzistuoja? " + failas.exists());
        
//        FileInputStream fis1 = new FileInputStream(failas);
//        InputStreamReader isr1 = new InputStreamReader(fis1, "UTF-8");
//        BufferedReader br1 = new BufferedReader(isr1);
        BufferedReader br1 = createBR(failas);   // metodas parasytas apacioje
        String failoturinys = readTextFile(br1);
        br1.close();
        
//        FileOutputStream fos = new FileOutputStream(failas);
//        OutputStreamWriter osw = new OutputStreamWriter(fos,"UTF-8");
//        BufferedWriter bw = new BufferedWriter(osw);
        BufferedWriter bw = createBW(failas);   // metodas parasytas apacioje
        bw.append(failoturinys);
        bw.newLine();
        bw.append("pirmas irasymas");
        bw.flush();
        bw.close();
        
        
        FileInputStream fis = new FileInputStream(failas);
        InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
        BufferedReader br = new BufferedReader(isr);
//        out("Ivesto failo pirma eilute: " + br.readLine());
//        out("Ivesto failo antra eilute: " + br.readLine());
        out("txt turinys: " + NL +readTextFile(br));
        
        
        br.close();
    } 
}